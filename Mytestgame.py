# Simple pygame program

# Import and initialize the pygame library
import pygame
pygame.init()

# Set up the drawing window
screen = pygame.display.set_mode([700, 500])

# Run until the user asks to quit
running = True
while running:

    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Fill the background with white
    screen.fill((255, 255, 255))

    # Draw a solid blue circle in the center
    pygame.draw.circle(screen, (0, 0, 255), (100, 250), 60)

    # Another one
    pygame.draw.circle(screen, (0, 255, 255), (230, 150), 60)
    
    # ANohter one
    pygame.draw.circle(screen, (255, 255, 0), (360, 250), 60)

    #Jaysus Crust
    pygame.draw.circle(screen, (255, 0, 0), (490, 150), 60)

    #Meh
    pygame.draw.circle(screen, (0, 0, 255), (620, 250), 60)


    # Flip the display
    pygame.display.flip()

# Done! Time to quit.
pygame.quit()
